colo pablo
syntax on
set ignorecase
set smartcase

" allow edited buffers to be hidden
set hidden

" highlight current line
set cursorline

" max line-width column marker
:set colorcolumn=100

" hightlight trailing whitespace
set list
set listchars=trail:*

" remap ; to :
nnoremap ; :

" disable syntax highlighting in vimdiff
if &diff
    syntax off
endif